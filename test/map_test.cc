/*******************************************************************************
 * Copyright (c) 2023, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "mantle_api/map.h"

#include <gtest/gtest.h>

#include "mantle_api/lane.h"
#include "mantle_api/road.h"
#include "map_adapter/map_adapter.h"
#include "map_impl/chunk.h"
#include "map_impl/lane.h"
#include "map_impl/map.h"

TEST(TemplateInterfaceTest, MapTest) {
  auto map_impl = map_impl::Map{};
  auto map = mantle_api::Map{map_adapter::MapAdapter{map_impl}};

  // Prefill the map
  //
  // Filling the map impl with data would usually be done by the map impl itself
  {
    auto lane = map_impl::Lane{};
    lane.SetIdentifier("1");
    lane.SetWidth(4.20);

    auto chunk = map_impl::Chunk{};
    chunk.SetIdentifier("1");
    chunk.AddLane(lane);

    map_impl.AddChunk(chunk);
  }

  // Query the map
  //
  // Note that this is only "generic" at compile time
  // All types will be deduced at compile time and are therefore concrete at runtime
  auto road_fail = map.GetRoad(42);
  ASSERT_FALSE(road_fail);

  auto road_pass = map.GetRoad(1);
  ASSERT_TRUE(road_pass);

  auto lane_fail = road_pass->GetLane(42);
  ASSERT_FALSE(lane_fail);

  auto lane_pass = road_pass->GetLane(1);
  ASSERT_TRUE(lane_pass);

  auto width = lane_pass->GetWidth();
  ASSERT_DOUBLE_EQ(width, 4.20);
}
