Template Interface PoC

1. `mantle_api` is the interface part implemented in the MantleAPI repo
2. `map_adapter` contains the code that adapts some map library to the MantleAPI
3. `map_impl` is an example for any map library that we have limited or no control over
4. `test` shows how to use the adapted map library
