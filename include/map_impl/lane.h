/*******************************************************************************
 * Copyright (c) 2023, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <string>

namespace map_impl {

class Lane {
  std::string identifier_;
  double width_{};

 public:
  void SetIdentifier(const std::string& identifier) noexcept { identifier_ = identifier; }

  [[nodiscard]] auto GetIdentifier() const noexcept { return identifier_; }

  void SetWidth(double width) noexcept { width_ = width; }

  [[nodiscard]] auto GetWidth() const noexcept { return width_; }
};

}  // namespace map_impl
