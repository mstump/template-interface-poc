/*******************************************************************************
 * Copyright (c) 2023, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <algorithm>
#include <string>
#include <vector>

#include "map_impl/lane.h"

namespace map_impl {

class Chunk {
  std::string identifier_;
  std::vector<Lane> lanes_;

 public:
  void SetIdentifier(const std::string& identifier) noexcept { identifier_ = identifier; }

  [[nodiscard]] auto GetIdentifier() const noexcept { return identifier_; }

  void AddLane(const Lane& lane) noexcept { lanes_.push_back(lane); }

  [[nodiscard]] auto GetLane(std::string identifier) noexcept -> std::optional<std::reference_wrapper<Lane>> {
    auto iter = std::find_if(std::begin(lanes_), std::end(lanes_),
                             [&identifier](const auto& lane) { return lane.GetIdentifier() == identifier; });
    if (iter != std::end(lanes_)) {
      return *iter;
    }

    return std::nullopt;
  };
};

}  // namespace map_impl
