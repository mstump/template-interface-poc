/*******************************************************************************
 * Copyright (c) 2023, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <functional>
#include <string>
#include <vector>

#include "map_impl/chunk.h"

namespace map_impl {

class Map {
  std::vector<Chunk> chunks_;

 public:
  void AddChunk(const Chunk& chunk) noexcept { chunks_.push_back(chunk); }

  [[nodiscard]] auto GetChunk(std::string identifier) noexcept -> std::optional<std::reference_wrapper<Chunk>> {
    auto iter = std::find_if(std::begin(chunks_), std::end(chunks_),
                             [&identifier](const auto& chunk) { return chunk.GetIdentifier() == identifier; });
    if (iter != std::end(chunks_)) {
      return *iter;
    }

    return std::nullopt;
  };
};

}  // namespace map_impl
