/*******************************************************************************
 * Copyright (c) 2023, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include "mantle_api/lane.h"

namespace mantle_api {

template <typename T>
class Road {
  T road_impl_;

 public:
  Road() = default;

  explicit Road(T road_impl) : road_impl_(road_impl) {}

  void SetIdentifier(int identifier) noexcept { road_impl_.SetId(identifier); }

  [[nodiscard]] auto GetIdentifier() const noexcept { return road_impl_.GetId(); }

  [[nodiscard]] auto GetLane(int identifier) { return road_impl_.GetLane(identifier); }

  [[nodiscard]] auto Get() const noexcept -> const T& { return road_impl_; }
};

}  // namespace mantle_api
