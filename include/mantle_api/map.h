/*******************************************************************************
 * Copyright (c) 2023, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include "mantle_api/road.h"

namespace mantle_api {

template <typename T>
class Map {
  T map_impl_;

 public:
  Map() = default;

  explicit Map(T map_impl) : map_impl_(map_impl) {}

  [[nodiscard]] auto GetRoad(int identifier) { return map_impl_.GetRoad(identifier); }
};

}  // namespace mantle_api
