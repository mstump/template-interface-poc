/*******************************************************************************
 * Copyright (c) 2023, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

namespace mantle_api {

template <typename T>
class Lane {
  T lane_impl_;

 public:
  Lane() = default;

  explicit Lane(T lane_impl) : lane_impl_(lane_impl) {}

  void SetIdentifier(int identifier) noexcept { lane_impl_.SetId(identifier); }

  [[nodiscard]] auto GetIdentifier() const noexcept { return lane_impl_.GetId(); }

  [[nodiscard]] auto GetWidth() { return lane_impl_.GetWidth(); }

  [[nodiscard]] auto Get() const noexcept -> const T& { return lane_impl_; }
};

}  // namespace mantle_api
