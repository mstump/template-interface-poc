/*******************************************************************************
 * Copyright (c) 2023, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <optional>

#include "mantle_api/lane.h"
#include "map_adapter/lane_adapter.h"
#include "map_impl/chunk.h"
#include "map_impl/lane.h"

namespace map_adapter {

class RoadAdapter {
  map_impl::Chunk& chunk_;

 public:
  explicit RoadAdapter(map_impl::Chunk& chunk) : chunk_(chunk) {}

  void SetIdentifier(int identifier) noexcept { chunk_.SetIdentifier(std::to_string(identifier)); }

  [[nodiscard]] [[nodiscard]] auto GetIdentifier() const noexcept { return std::stoi(chunk_.GetIdentifier()); }

  void AddLane(const mantle_api::Lane<LaneAdapter>& lane) noexcept { chunk_.AddLane(lane.Get().Get()); }

  [[nodiscard]] auto GetLane(int identifier) -> std::optional<mantle_api::Lane<LaneAdapter>> {
    if (auto lane = chunk_.GetLane(std::to_string(identifier))) {
      return mantle_api::Lane{LaneAdapter{lane.value()}};
    }

    return std::nullopt;
  }

  [[nodiscard]] auto Get() const noexcept -> const map_impl::Chunk& { return chunk_; }
};

}  // namespace map_adapter
