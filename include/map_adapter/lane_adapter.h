/*******************************************************************************
 * Copyright (c) 2023, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include "map_impl/lane.h"

namespace map_adapter {

class LaneAdapter {
  map_impl::Lane& lane_;

 public:
  explicit LaneAdapter(map_impl::Lane& lane) : lane_(lane) {}

  void SetIdentifier(int identifier) noexcept { lane_.SetIdentifier(std::to_string(identifier)); }

  [[nodiscard]] auto GetIdentifier() const noexcept { return std::stoi(lane_.GetIdentifier()); }

  [[nodiscard]] auto GetWidth() { return lane_.GetWidth(); }

  [[nodiscard]] auto Get() const noexcept -> const map_impl::Lane& { return lane_; }
};

}  // namespace map_adapter
