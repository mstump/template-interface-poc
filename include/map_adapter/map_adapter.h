/*******************************************************************************
 * Copyright (c) 2023, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <optional>

#include "mantle_api/road.h"
#include "map_adapter/road_adapter.h"
#include "map_impl/map.h"

namespace map_adapter {

class MapAdapter {
  map_impl::Map& map_;

 public:
  explicit MapAdapter(map_impl::Map& map) : map_(map) {}

  // Note how we translate between the MantleAPI and the map implementation:
  // * The `identifier` is converted from `int` to `std::string`
  // * `GetRoad` is translated to `GetChunk`
  // * The `Chunk` is wrapped in a `RoadAdapter` which in turn is wrapped in a `mantle_api::Road`
  [[nodiscard]] auto GetRoad(int identifier) -> std::optional<mantle_api::Road<RoadAdapter>> {
    if (auto chunk = map_.GetChunk(std::to_string(identifier))) {
      return mantle_api::Road{RoadAdapter{chunk.value()}};
    }

    return std::nullopt;
  }

  [[nodiscard]] auto Get() const noexcept -> const map_impl::Map& { return map_; }
};

}  // namespace map_adapter
